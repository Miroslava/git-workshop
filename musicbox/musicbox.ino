#include "pitches.h"
#include "button.h"
#include "buzzer.h"

#define PIN_BUTTON_MEL_ONE 3
#define PIN_BUTTON_OFF 5
#define PIN_BUTTON_SPEED 4
#define PIN_BUZZER 6

Button buttonMelodySelect(PIN_BUTTON_MEL_ONE);
Button buttonOff(PIN_BUTTON_OFF);
Button buttonSpeed(PIN_BUTTON_SPEED);
Buzzer buzzer(PIN_BUZZER);
//Button buttonMelodySelect();

int notes[] = {NOTE_A4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE};
double durations[] = {8, 1, 4, 1};
int melodyLength = 4;

#define PIN_BUTTON_MEL_TWO 4
Button buttonMelodyTwo(PIN_BUTTON_MEL_TWO);

int notes2[] = {NOTE_C4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE};
double durations2[] = {4, 1, 4, 1};
int melodyLength2 = 4;

int notes3[] = {NOTE_F1, NOTE_SILENCE, NOTE_B3, NOTE_SILENCE, 
                NOTE_B3, NOTE_SILENCE, NOTE_C7, NOTE_SILENCE, 
                NOTE_F1, NOTE_SILENCE, NOTE_F1, NOTE_SILENCE, 
                NOTE_B3, NOTE_SILENCE, NOTE_B7, NOTE_SILENCE, 
                NOTE_E7, NOTE_SILENCE, NOTE_A7, NOTE_SILENCE};
double durations3[] = {2, 1, 2, 1, 
                       1, 1, 1, 1, 
                       2, 1, 2, 1,
                       1, 1, 1, 1,
                       2, 1, 2, 1};
int melodyLength3 = 20;

unsigned long speeds[] = {25, 50, 100, 200, 400, 800};
int currentSpeed = 2;
int speedsLength = 6;

int currentMelody = 0;
int melodyLengthSpeed = 3;

void setup() {
  buzzer.setMelody(notes, durations, melodyLength);
}

void loop() {
  buzzer.playSound();

  if (buttonOff.wasPressed()) {
    buzzer.turnSoundOff();
  }

  //  if (buttonMelodyOne.wasPressed()) {
  //    buzzer.setMelody(notes, durations, melodyLength);
  //    buzzer.turnSoundOn();
  //  }

  if (buttonSpeed.wasPressed()) {
    currentSpeed = (currentSpeed + 1) % speedsLength;
    buzzer.setNoteDuration(speeds[currentSpeed]);
  }

  if (buttonMelodySelect.wasPressed()) {
    if (currentMelody == 0)    {
      buzzer.setMelody(notes, durations, melodyLength);
      buzzer.turnSoundOn();
    }
    if (currentMelody == 1)    {
      buzzer.setMelody(notes2, durations2, melodyLength2);
      buzzer.turnSoundOn();
    }
    if (currentMelody == 2)    {
      buzzer.setMelody(notes3, durations3, melodyLength3);
      buzzer.turnSoundOn();
    }

    currentMelody = (currentMelody + 1) % melodyLengthSpeed;
  }
}
